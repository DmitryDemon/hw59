import React, {Component} from 'react';
import './ToDo.css';

class Task extends Component{

    shouldComponentUpdate(nextProps){
        return this.props.value !== nextProps.value;
    }

    render (){
        console.log("mount");
        return (
                <div className='task'>
                    <input
                        type="text"
                        className='kinoName'
                        value={this.props.value}
                        onChange={this.props.changed}
                    />
                    <button onClick={this.props.remove} className='btn-remove'>Remove</button>
                </div>
        )
    }

}

export default Task;